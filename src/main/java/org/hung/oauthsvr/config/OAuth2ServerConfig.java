package org.hung.oauthsvr.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
public class OAuth2ServerConfig {

	@Configuration
	@EnableResourceServer
	protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

		@Override
		public void configure(ResourceServerSecurityConfigurer resources) {
			resources.resourceId("userinfo").stateless(false);
		}

		@Override
		public void configure(HttpSecurity http) throws Exception {
			// @formatter:off
			http
			 // Since we want the protected resources to be accessible in the UI as well we
			 // need session creation to be allowed (it's disabled by default in 2.0.6)
			  .sessionManagement()
			    .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
			  .and()
			    .authorizeRequests()
			      .antMatchers("/oauth/userinfo")
					.authenticated();
			// @formatter:on
		}
	}

	@Configuration
	@EnableAuthorizationServer
	protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

		// @Autowired
		// private TokenStore tokenStore;

		@Autowired
		private AuthenticationManager authenticationManager;
		
		@Autowired
		private UserDetailsService userDetailsService;

		@Override
		public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
			security
			  .checkTokenAccess("permitAll()");
		}

		
		@Override
		public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {			
			//Reference: (Grant Types) https://projects.spring.io/spring-security-oauth/docs/oauth2.html
			endpoints
 			  //Inject the authenticationManager to switch on the password grant type
			  .authenticationManager(authenticationManager)
			  //if you inject a UserDetailsService or if one is configured globally anyway (e.g. in a GlobalAuthenticationManagerConfigurer) 
			  //then a refresh token grant will contain a check on the user details, to ensure that the account is still active
			  .userDetailsService(userDetailsService);
		}


		@Override
		public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
			// @formatter:off
			clients
			  .inMemory()
			    .withClient("zuul-api-gateway")
 			      .secret("{noop}password")
			      .resourceIds("userinfo", "dummy")
			      .authorizedGrantTypes("authorization_code")
			      .authorities("ROLE_USER")
			      .scopes("read","delete")
                  .additionalInformation("applnCode", "CF")
                  .autoApprove(false)
                  .redirectUris(
                  	 "http://localhost:8080/oauth2/login"
                  )
                .and()
                  .withClient("eod")
                    .authorizedGrantTypes("password")
                    .secret("{noop}pwd")
                .and()
                  .withClient("plain-client-1")
                    .secret("{noop}password")
                    .authorizedGrantTypes("authorization_code")
                    .accessTokenValiditySeconds(5)
                    .scopes("select","insert","update","delete")
                    .redirectUris(
                      "http://localhost:8080/login/oauth2/code/my-oauth-client1"
                    )
                .and()
                  .withClient("plain-client-2")
					.secret("{noop}password")
					.authorizedGrantTypes("authorization_code","refresh_token","password")
					.accessTokenValiditySeconds(5)
					.refreshTokenValiditySeconds(15)
					.scopes("select","insert","update","delete")
					.autoApprove(true)
					.autoApprove("select")
					.redirectUris(
					  "http://localhost:8080/login",
					  "http://localhost:8080/caller"
					);
			
			// @formatter:on
		}

		// @Bean
		// public TokenStore tokenStore() {
		// return new InMemoryTokenStore();
		// }

	}

}
