package org.hung.oauthsvr.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.debug(true);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/error").permitAll()
			.anyRequest().authenticated()
			.and().httpBasic();
		//http.authorizeRequests().anyRequest().permitAll();
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
			.withUser("user").password("{noop}password").roles("USER","ADMIN")
		.and()
			.withUser("john").password("{noop}password").roles("OPERATOR")
		.and()
			.withUser("server1").password("{noop}password").roles("SERVER");
			//.and().withUser("client1").password("{noop}abcd1234").roles("CLIENT")
			;
	}

	@Bean(name="authenticationManager")
	@Override
	//Expose the AuthenticationManager created from "configure(AuthenticationManagerBuilder)" as Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return this.authenticationManager();
	}

	@Bean
	@Override
	//Expose the UserDetailService created from "configure(AuthenticationManagerBuilder)" as Bean
	public UserDetailsService userDetailsServiceBean() throws Exception {
		return super.userDetailsServiceBean();
	}

}
