package org.hung.oauthsvr.web;

import java.security.Principal;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class UserInfoController {

    @RequestMapping("/oauth/userinfo")    
    public UserDetails user(Principal principal) {
    	log.info(principal.toString());
    	UserDetails userdetails = null;
    	if (principal instanceof UsernamePasswordAuthenticationToken) {
    		userdetails = (UserDetails)((UsernamePasswordAuthenticationToken)principal).getPrincipal();
    	} else if (principal instanceof OAuth2Authentication) {
    		userdetails = (UserDetails)((OAuth2Authentication)principal).getUserAuthentication().getPrincipal();
    	}
    	return userdetails;
    }
	
}
