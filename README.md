curl -v \
  --user eod:pwd \
  --url http://localhost:7070/oauth/token \
  --request POST\
  -d grant_type=password \
  -d username=server1 \
  -d password=password \
  -d scope=read

curl -v -L \
  --url http://server1:password@localhost:8080/echo/greeting \
  --request GET